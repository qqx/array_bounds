'''Given an asc sorted array and a value, determine between which two array items the
value falls.'''


def get_bounds(asc_sorted_array, value, boundary_up = True):
    '''boundary up means that if the value falls on a boundary it 
    is considered to be part of the upper array interval.
    Returns None if not found.
    '''
    def boundary_up_predicate(lower, upper, value):
        if lower <= value < upper:
            return True
        else:
            return False
  
    def boundary_down_predicate(lower, upper, value):
        if lower < value <= upper:
            return True
        else:
            return False

    predicate = boundary_up_predicate
    if not boundary_up:
        predicate = boundary_down_predicate

    for i in range(len(asc_sorted_array)-1):
        j = i+1
        
        lower = asc_sorted_array[i]
        upper = asc_sorted_array[j]

        if predicate(lower, upper, value):
            return lower, upper


if __name__ == '__main__':
    import unittest

    class TestBetterArray(unittest.TestCase):

        def setUp(self):
            self.a1 = [10, 14, 19, 20, 21, 22, 23, 28, 40]
        
        def test_simple(self):
            v1 = 29
            result = get_bounds(self.a1, v1)
            expected_result = (28, 40)
            self.assertEqual(expected_result, result)

        def test_boundary_up(self):
            v1 = 28
            result = get_bounds(self.a1, v1)
            expected_result = (28, 40)
            self.assertEqual(expected_result, result)

        def test_boundary_up_top(self):
            v1 = 40
            result = get_bounds(self.a1, v1)
            expected_result = None
            self.assertEqual(expected_result, result)

        def test_boundary_up_bottom(self):
            v1 = 10
            result = get_bounds(self.a1, v1)
            expected_result = (10, 14)
            self.assertEqual(expected_result, result)

        def test_boundary_up_over(self):
            v1 = 41
            result = get_bounds(self.a1, v1)
            expected_result = None
            self.assertEqual(expected_result, result)

        def test_boundary_up_under(self):
            v1 = 9
            result = get_bounds(self.a1, v1)
            expected_result = None
            self.assertEqual(expected_result, result)

        def test_simple_x(self):
            v1 = 29
            result = get_bounds(self.a1, v1, boundary_up=False)
            expected_result = (28, 40)
            self.assertEqual(expected_result, result)

        def test_boundary_up_x(self):
            v1 = 28
            result = get_bounds(self.a1, v1, boundary_up=False)
            expected_result = (23, 28)
            self.assertEqual(expected_result, result)

        def test_boundary_up_top_x(self):
            v1 = 40
            result = get_bounds(self.a1, v1, boundary_up=False)
            expected_result = (28, 40)
            self.assertEqual(expected_result, result)

        def test_boundary_up_bottom_x(self):
            v1 = 10
            result = get_bounds(self.a1, v1, boundary_up=False)
            expected_result = None
            self.assertEqual(expected_result, result)

        def test_boundary_up_over_x(self):
            v1 = 41
            result = get_bounds(self.a1, v1, boundary_up=False)
            expected_result = None
            self.assertEqual(expected_result, result)

        def test_boundary_up_under_x(self):
            v1 = 9
            result = get_bounds(self.a1, v1, boundary_up=False)
            expected_result = None
            self.assertEqual(expected_result, result)


    unittest.main()

